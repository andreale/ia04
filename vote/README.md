# IA04 Serveur de vote (Alexandre André & Nicolas Schlaflang)

## Comment lancer le server 
### Récupération du module
Créer un module ou se placer dans un dossier avec un fichier _go.mod_ puis lancer la commande `go get`
```bash
# En option si on a deja un fichier go.mod
go mod init monsupermodule
# récupération
go get gitlab.utc.fr/andreale/ia04@v1.0.6
```
### Installation et lancement du server avec des agents qui votent dessus
```bash
# installation
go install gitlab.utc.fr/andreale/ia04/vote/cmd/launchallagentsAndreSchlaflang@v1.0.6
# lancement 
launchallagentsAndreSchlaflang
```
### Installation du server simple (en cas de test de l'API)
```bash
#installation
go install gitlab.utc.fr/andreale/ia04/vote/cmd/launchservAndreSchlaflang@v1.0.6
# lancement
launchservAndreSchlaflang
```



## Ce que nous avons implémenté
L'architecture du projet est la suivante:


```
vote
├── agt
│   ├── restserveragent
│   └── voteragent
├── cmd
├── comsoc
```

Dans le dossier _agt/restserveragent_ nous avons implémenté, le serveur dans de vote et les 3 méthodes demandées:

### Commande *"/new_ballot"*:

On filtre la requête pour créer un nouveau scrutin et on contrôle qu'elle est cohérente.
Les contrôles suivants sont effectués:
- La méthode HTTP est bien POST
- Le parsing du JSON ne pose pas de problème 
- Les ids des votants sont uniques
- Le tie-break est cohérent avec le nombre d'alternatives fournies (la longueur du tableau est la même et chaque alternative est dans le tie-break)
- Le format de l'heure de fin du scrutin est bien au format RFC399
- Le format de l'heure de fin du scrutin est après le temps de création

En cas de non respect de ces conditions, on renvoie une erreur 400

On contrôle également que la règle de vote est bien implémentée sinon on revoie une erreur 501

Si tous les contrôles sont passés, on modifie l'état du serveur en créant un nouveau scrutin dont on retourne l'id (dans un JSON) et on renvoie le code 201.

### Commande *"/vote"*:
On filtre la requête pour ajouter un vote à un scrutin et on contrôle qu'elle est cohérente.

Les contrôles suivants sont effectués:
- La méthode HTTP est bien POST
- Le parsing du JSON ne pose pas de problème
- L'id du scrutin fourni correspond bien à un scrutin créé par le serveur
- Le votant est autorisé à voter
- La liste des alternatives fournie par le votant est cohérente (toutes les alternatives présentes une et une unique fois)
- Pour le vote par approval on vérifie que l'option donnée est cohérente (un tableau de longueur 1 et qui représente bien un nombre entre 1 et le nombre d'alternatives)

En cas de non respect de ces conditions on renvoie une erreur 400

On vérifie aussi que le votant n'a pas dépasser la dealine de vote sinon on revoie une erreur 503.

On vérifie également que le votant n'a pas déjà voté sinon on renvoie l'erreur 403.

Si tous les contrôles sont passés, on revoie le code 200.

### Commande *"/result"*:
On filtre la requête pour renvoyer le résultat d'un vote.

Les contrôles suivants sont effectués:
- La méthode HTTP est bien POST et le parsing du JSON ne pose pas de problème (sinon erreur 400)
- L'id du scrutin fourni correspond bien à un scrutin créé par le serveur (sinon erreur 404)
- Le scrutin est terminé (sinon erreur 425)

Pour obtenir le résultat du scrutin on utilise les fonctions factories de _comsoc/factory.go_ et les SCF/SWF dans les fichiers _comsoc/[methode_vote].go_ pour générer les SWF, SCF pour le scrutin en question.

A noter qu'on stocke dans des variables (`winner` et `ranking`) associées à la structure d'un scrutins (`ballot`) les résultats d'un vote la première fois qu'on fait une requête dessus. Cela permet de ne pas rappeler toutes les fonctions de _comsoc_ caque fois.

A noter qu'on appelle pas les mêmes factories si on a une méthode de qui requière des options (example approval) ou non.

Si tous les contrôles sont passés on renvoie le code 200 et le gagnant du scrutin ainsi que la liste des alternatives rangées par ordre de préférence des votants le tout départagé par le tie-break.

### Fichier *restserveragent/serv_test.go*
Dans ce fichier on retrouve un ensemble complet de tests unitaires pour vérifier chaque condition de contrôle citée précédemment. Par exemple la fonction `TestVoteTooLate(t *testing.T)` permet de tester que l'erreur 503 est bien renvoyée quand un votant vote trop tard (en jouant avec un `time.Sleep`).

Le serveur a été développé en parallèle des tests unitaires pour s'assurer que les fonctionnalités implémentées seraient consistantes jusqu'à la fin du développement.

### Dossier *agt/voteragent*
Dans le fichier _voter.go_ est définie un agent votant avec toutes ses méthodes pour créer un scrutin, voter ou demander le résultat d'un vote. 

### dossier *cmd*

le dossier cmd contient les exécutables ,il est composé deux des sous dossier _lauchserv_ et _lauchallagents_.

#### Package *cmd/launchallagentsAndreSchlaflang*

Ce dossier permet de lancer le serveur de vote en mode local, en attendant les requêtes des autres agents sur le port 8080


#### Package *cmd/launchallagentsAndreSchlaflang*

 Le fichier _launchallagents.go_ effectue une demos du projet , il permet de lancer un serveur de vote en mode local et d'inviter des agents à voter sur ce serveur. Lorsque ce fichier est exécuté, il effectue les actions suivantes :

1. Création de plusieurs agents votants : Le fichier génère un ensemble d'agents votants, chacun étant configuré avec un identifiant unique, prêt à participer aux votes.

2. Démarrage du serveur : Il lance le serveur de vote en local sur le port 8080, permettant aux agents votants d'envoyer leurs votes.

3. Lancement des votes : Pour chaque méthode de vote implémentée (majority, borda, stv, copeland, approval), le fichier crée un vote avec des règles spécifiques, un nombre aléatoire d'agents votants, et un ensemble aléatoire d'alternatives. Les votes sont ensuite soumis par les agents votants.

4. Calcul des résultats : Une fois les votes soumis, le fichier demande au serveur de calculer les résultats des différentes méthodes de vote. Les résultats sont ensuite affichés, y compris le gagnant du scrutin, le classement des alternatives

### dossier *comsoc*
Se trouve chacune des méthodes de votes implémentées:
- Single transferable vote (stv)
- Vote majoritaire (majority)
- Copeland
- Borda
- Vote par approbation (approval)

Les méthodes de vote sont toutes testées dans le fichier issu du td `comsoc/cs_test.go` qui a été compléter pour couvrir tous les votes.

Pour créer des résultats où tous les candidats sont départagés pour la SWF et où on est sûr d'obtenir un gagnant pour la SCF on utilise les tie-breaks. Ils sont utilisés avec des factories pour obtenir le résultat voulu. 

A noter que pour les méthodes qui prennent une option (par example approval) des factories différentes où les paramètres des fonctions ont été modifiés sont ajoutées 

```go
func SWFFactoryOptions(swf func(p Profile, options [][]int) (Count, error), tiebreak func([]Alternative) (Alternative, error)) func(Profile, [][]int) ([]Alternative, error)
```
```go
func SCFFactoryOptions(scf func(p Profile, options [][]int) ([]Alternative, error), tiebreak func([]Alternative) (Alternative, error)) func(Profile, [][]int) (Alternative, error) 
```
On remarque bien le `[][]int` pour prendre en compte le seuil d'acceptation d'approval par exemple. 

Il aurait pu aussi avoir une seule type de factories et ajouter des options inutilisées pour toutes les autres méthodes de vote. Nous n'avons pas choisie cette options car nous ne savions pas si les fichiers d'évaluation sur nos méthodes de vote étaient compatibles avec ce changement.

Par rapport au TD où ces méthodes ont été implémenté les fonctions d'approval prennait l'argument `[]int` en option, il a été modifier pour être cohérent avec l'API en `[][]int`.
