package vote

import (
	comsoc "gitlab.utc.fr/andreale/ia04/vote/comsoc"
)

type NewBallotRqst struct {
	Rule      string               `json:"rule"`
	EndDate   string               `json:"deadline"`
	VotersIds []string             `json:"voter-ids"`
	NumbAlts  int                  `json:"#alts"`
	TieBreak  []comsoc.Alternative `json:"tie-break"`
}

type VoteRqst struct {
	AgentId  string               `json:"agent-id"`
	BallotId string               `json:"ballot-id"`
	Prefs    []comsoc.Alternative `json:"prefs"`
	Option   []int                `json:"options,omitempty"`
}

type ResultRqst struct {
	BallotId string `json:"ballot-id"`
}

type NewBallotResp struct {
	BallotId string `json:"ballot-id"`
}

type VoteResp struct {
	Winner  comsoc.Alternative   `json:"winner"`
	Ranking []comsoc.Alternative `json:"ranking,omitempty"`
}
