package comsoc

func CondorcetWinner(p Profile) (bestAlts []Alternative, err error) {
	var alts []Alternative

	// on prend la liste des alternatives possibles
	for _, a := range p[0] {
		alts = append(alts, a)
	}

	err = checkProfileAlternative(p, alts)
	if err != nil {
		return []Alternative{}, err
	}

	// on teste si une alternative alts[i] est majoritairement préférée aux autres alternatives alts[j]
	// quand on restreint le profil à juste alts[i] et alts[j]
	for i := 0; i < len(alts); i++ {
		count := 0
		for j := 0; j < len(alts); j++ {
			for _, a := range p {
				if i != j {
					if isPref(alts[i], alts[j], a) {
						count += 1
					} else {
						count -= 1
					}
				}
			}
		}

		if count > 0 {
			return []Alternative{alts[i]}, nil
		}
	}

	return []Alternative{}, nil

}
