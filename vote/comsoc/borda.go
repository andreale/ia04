package comsoc

func BordaSWF(p Profile) (count Count, err error) {
	alts := []Alternative{}
	count = make(Count)

	// initialisation à 0 du count pour chaque alternative
	for _, a := range p[0] {
		alts = append(alts, a)
		count[a] = 0
	}

	err = checkProfileAlternative(p, alts)
	if err != nil {
		return count, err
	}
	num_alts := len(alts)
	// pour chaque alternative on associe un score
	// en fonction de sa position dans les préférences
	for _, prefs := range p {
		for _, alt := range alts {
			count[alt] += num_alts - rank(alt, prefs) - 1
		}
	}

	return count, nil
}

func BordaSCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := BordaSWF(p)
	return maxCount(count), err
}
