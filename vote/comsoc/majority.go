package comsoc

func MajoritySWF(p Profile) (count Count, err error) {
	alts := []Alternative{}
	count = make(Count)

	for _, a := range p[0] {
		alts = append(alts, a)
		count[a] = 0
	}

	err = checkProfileAlternative(p, alts)
	if err != nil {
		return count, err
	}

	for _, a := range p {
		count[a[0]]++
	}

	return count, nil
}

func MajoritySCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := MajoritySWF(p)
	return maxCount(count), err
}
