package comsoc

import "fmt"
import "math"

func TieBreakFactory(orderedAlts []Alternative) func([]Alternative) (Alternative, error) {
	return func(alts []Alternative) (Alternative, error) {
		if len(orderedAlts) == 0 {
			return -1, fmt.Errorf("Ordered sclice of alternatives is empty")
		}
		if len(alts) == 1 {
			return alts[0], nil
		}
		for _, orderedAlt := range orderedAlts {
			for _, alt := range alts {
				if orderedAlt == alt {
					return alt, nil
				}
			}
		}
		return -1, fmt.Errorf("No alternatives given in parameters are in the tie break")
	}
}

func SWFFactory(swf func(p Profile) (Count, error), tiebreak func([]Alternative) (Alternative, error)) func(Profile) ([]Alternative, error) {
	return func(p Profile) ([]Alternative, error) {
		count, err := swf(p)

		if err != nil {
			return nil, err
		}

		if len(count) == 0 {
			return nil, fmt.Errorf("Ordered sclice of alternatives is empty")
		}

		res := make([]Alternative, 0)
		// sure that the loop stops  because each iteration a key of the map count is deleted
		for len(count) > 0 {
			higestScore := math.MinInt
			candidates := make([]Alternative, 0)
			for k, v := range count {
				if v > higestScore {
					higestScore = v
					candidates = make([]Alternative, 0)
					candidates = append(candidates, k)
				}
				if v == higestScore {
					candidates = append(candidates, k)
				}
			}

			winner, err := tiebreak(candidates)

			if err != nil {
				return nil, err
			}

			res = append(res, winner)
			delete(count, winner)
		}
		return res, nil
	}
}

func SCFFactory(scf func(p Profile) ([]Alternative, error), tiebreak func([]Alternative) (Alternative, error)) func(Profile) (Alternative, error) {
	return func(p Profile) (Alternative, error) {
		alts, err := scf(p)

		if err != nil {
			return -1, err
		}

		a, err := tiebreak(alts)

		if err != nil {
			return -1, err
		}

		return a, nil
	}
}

func SWFFactoryOptions(swf func(p Profile, options [][]int) (Count, error), tiebreak func([]Alternative) (Alternative, error)) func(Profile, [][]int) ([]Alternative, error) {
	return func(p Profile, options [][]int) ([]Alternative, error) {
		count, err := swf(p, options)

		if err != nil {
			return nil, err
		}

		if len(count) == 0 {
			return nil, fmt.Errorf("Ordered sclice of alternatives is empty")
		}

		res := make([]Alternative, 0)
		// sure that the loop stops  because each iteration a key of the map count is deleted
		for len(count) > 0 {
			higestScore := math.MinInt
			candidates := make([]Alternative, 0)
			for k, v := range count {
				if v > higestScore {
					higestScore = v
					candidates = make([]Alternative, 0)
					candidates = append(candidates, k)
				}
				if v == higestScore {
					candidates = append(candidates, k)
				}
			}
			winner, err := tiebreak(candidates)

			if err != nil {
				return nil, err
			}

			res = append(res, winner)
			delete(count, winner)
		}

		return res, nil
	}
}

func SCFFactoryOptions(scf func(p Profile, options [][]int) ([]Alternative, error), tiebreak func([]Alternative) (Alternative, error)) func(Profile, [][]int) (Alternative, error) {
	return func(p Profile, options [][]int) (Alternative, error) {
		alts, err := scf(p, options)

		if err != nil {
			return -1, err
		}

		a, err := tiebreak(alts)

		if err != nil {
			return -1, err
		}

		return a, nil
	}
}
