package comsoc

type Alternative int
type Profile [][]Alternative
type Count map[Alternative]int

var RulesImplemented = [...]string{"stv", "majority", "copeland", "borda", "approval"}
