package comsoc

func CopelandSWF(p Profile) (Count, error) {
	alts := []Alternative{}
	count := make(Count)

	// initialisation à 0 du count pour chaque alternative
	for _, a := range p[0] {
		alts = append(alts, a)
		count[a] = 0
	}

	err := checkProfileAlternative(p, alts)
	if err != nil {
		return count, err
	}

	for i := 0; i < len(alts); i++ {
		for j := i; j < len(alts); j++ {
			score := 0
			for _, a := range p {
				if i != j {
					if isPref(alts[i], alts[j], a) {
						score += 1
					} else {
						score -= 1
					}
				}
			}
			if score > 0 {
				count[alts[i]] += 1
				count[alts[j]] -= 1
			} else if score < 0 {
				count[alts[i]] -= 1
				count[alts[j]] += 1
			}
		}
	}
	return count, nil
}

func CopelandSCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := CopelandSWF(p)
	return maxCount(count), err
}
