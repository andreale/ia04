package comsoc

func STV_SWF(p Profile) (Count, error) {
	alts := []Alternative{}
	score := make(Count)
	count := make(Count)

	for _, a := range p[0] {
		alts = append(alts, a)
		score[a] = 0
		count[a] = 0
	}

	err := checkProfileAlternative(p, alts)
	if err != nil {
		return count, err
	}

	maj_tresh := int(len(p)/2) + 1
	nb_rounds := len(alts)
	round := 1

	for round < nb_rounds {
		// calcule du score pour chaque alternative restante
		for _, alt := range p {
			for _, a := range alt {
				_, exist := score[a]
				if exist {
					score[a] += 1
					break
				}
			}
		}

		min_key := []Alternative{}
		for k, v := range score {
			// l'alternative k a une majorité absolue
			if v >= maj_tresh {
				for k := range score {
					count[k] = round
				}
				count[k] = nb_rounds
				return count, nil
			}
			if len(min_key) == 0 {
				min_key = append(min_key, k)
			} else {
				if v == score[min_key[0]] {
					min_key = append(min_key, k)
				} else if v < score[min_key[0]] {
					min_key = []Alternative{k}
				}
			}
		}

		// s'il y au moins une alternative avec un score plus élévé que les autres le nombre d
		if len(min_key) < len(score) {
			// on retire les alternatives avec le plus faible score
			// on calcule egalement leur score dans count
			for _, k := range min_key {
				delete(score, k)
				count[k] = round
				break
			}

			//reset le score des alternatives encore en jeu
			for k := range score {
				score[k] = 0
			}
		}

		round += 1
	}
	return count, nil
}

func STV_SCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := STV_SWF(p)
	return maxCount(count), err
}
