package comsoc

func ApprovalSWF(p Profile, thresholds [][]int) (count Count, err error) {
	alts := []Alternative{}
	count = make(Count)

	for _, a := range p[0] {
		alts = append(alts, a)
		count[a] = 0
	}

	err = checkProfileAlternative(p, alts)
	if err != nil {
		return count, err
	}

	for i, a := range p {
		for j := 0; j < thresholds[i][0]; j++ {
			count[a[j]] += 1
		}
	}

	return count, nil
}

func ApprovalSCF(p Profile, thresholds [][]int) (bestAlts []Alternative, err error) {
	count, err := ApprovalSWF(p, thresholds)
	return maxCount(count), err
}
