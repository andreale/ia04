// version 2.0.0

package comsoc

import (
	"testing"
)

func TestBordaSWF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	res, _ := BordaSWF(prefs)

	t.Log(prefs)
	t.Log(res)

	if res[1] != 4 {
		t.Errorf("error, result for 1 should be 4, %d computed", res[1])
	}
	if res[2] != 3 {
		t.Errorf("error, result for 2 should be 3, %d computed", res[2])
	}
	if res[3] != 2 {
		t.Errorf("error, result for 3 should be 2, %d computed", res[3])
	}
}

func TestBordaSCF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	res, err := BordaSCF(prefs)

	if err != nil {
		t.Error(err)
	}

	t.Log(prefs)
	t.Log(res)

	if len(res) != 1 || res[0] != 1 {
		t.Errorf("error, 1 should be the only best Alternative")
	}
}

func TestMajoritySWF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	res, _ := MajoritySWF(prefs)

	t.Log(prefs)
	t.Log(res)

	if res[1] != 2 {
		t.Errorf("error, result for 1 should be 2, %d computed", res[1])
	}
	if res[2] != 0 {
		t.Errorf("error, result for 2 should be 0, %d computed", res[2])
	}
	if res[3] != 1 {
		t.Errorf("error, result for 3 should be 1, %d computed", res[3])
	}
}

func TestMajoritySCF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	res, err := MajoritySCF(prefs)

	if err != nil {
		t.Error(err)
	}

	t.Log(prefs)
	t.Log(res)

	if len(res) != 1 || res[0] != 1 {
		t.Errorf("error, 1 should be the only best Alternative")
	}
}

func TestApprovalSWF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 3, 2},
		{2, 3, 1},
	}
	thresholds := [][]int{{2}, {1}, {2}}

	res, _ := ApprovalSWF(prefs, thresholds)

	t.Log(prefs)
	t.Log(thresholds)
	t.Log(res)

	if res[1] != 2 {
		t.Errorf("error, result for 1 should be 2, %d computed", res[1])
	}
	if res[2] != 2 {
		t.Errorf("error, result for 2 should be 2, %d computed", res[2])
	}
	if res[3] != 1 {
		t.Errorf("error, result for 3 should be 1, %d computed", res[3])
	}
}

func TestApprovalSCF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 3, 2},
		{1, 2, 3},
		{2, 1, 3},
	}
	thresholds := [][]int{{2}, {1}, {2}}

	res, err := ApprovalSCF(prefs, thresholds)

	t.Log(prefs)
	t.Log(thresholds)
	t.Log(res)

	if err != nil {
		t.Error(err)
	}
	if len(res) != 1 || res[0] != 1 {
		t.Errorf("error, 1 should be the only best Alternative")
	}
}

func TestCondorcetWinner(t *testing.T) {
	prefs1 := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	prefs2 := [][]Alternative{
		{1, 2, 3},
		{2, 3, 1},
		{3, 1, 2},
	}

	res1, _ := CondorcetWinner(prefs1)
	res2, _ := CondorcetWinner(prefs2)

	t.Log(prefs1)
	t.Log(res1)
	t.Log(prefs2)
	t.Log(res2)

	if len(res1) == 0 || res1[0] != 1 {
		t.Errorf("error, 1 should be the only best alternative for prefs1")
	}
	if len(res2) != 0 {
		t.Errorf("no best alternative for prefs2")
	}
}

func TestCopelandSWF(t *testing.T) {
	prefs1 := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	prefs2 := [][]Alternative{
		{1, 2, 3},
		{2, 3, 1},
		{3, 1, 2},
	}

	res1, _ := CopelandSWF(prefs1)
	res2, _ := CopelandSWF(prefs2)

	t.Log(prefs1)
	t.Log(res1)
	t.Log(prefs2)
	t.Log(res2)

	if res1[1] != 2 {
		t.Errorf("error, result for 1 should be 2, %d computed", res1[1])
	}
	if res1[2] != 0 {
		t.Errorf("error, result for 2 should be 0, %d computed", res1[2])
	}
	if res1[3] != -2 {
		t.Errorf("error, result for 3 should be -2, %d computed", res1[3])
	}

	if res2[1] != 0 {
		t.Errorf("error, result for 1 should be 0, %d computed", res2[1])
	}
	if res2[2] != 0 {
		t.Errorf("error, result for 2 should be 0, %d computed", res2[2])
	}
	if res2[3] != 0 {
		t.Errorf("error, result for 3 should be 0, %d computed", res2[3])
	}
}

func TestCopelandSCF(t *testing.T) {
	prefs1 := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	prefs2 := [][]Alternative{
		{1, 2, 3},
		{2, 3, 1},
		{3, 1, 2},
	}

	res1, _ := CopelandSCF(prefs1)
	res2, _ := CopelandSCF(prefs2)

	t.Log(prefs1)
	t.Log(res1)
	t.Log(prefs2)
	t.Log(res2)

	if len(res1) == 0 || res1[0] != 1 {
		t.Errorf("error, 1 should be the only best alternative for prefs1")
	}
	if len(res2) != 3 {
		t.Errorf("no best alternative for prefs2")
	}
}

func TestSTV_SWF(t *testing.T) {
	prefs1 := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	prefs2 := [][]Alternative{
		{4, 2, 3, 1},
		{1, 2, 3, 4},
		{2, 3, 1, 4},
		{2, 3, 1, 4},
		{3, 1, 2, 4},
		{3, 2, 1, 4},
		{3, 2, 1, 4},
	}

	res1, _ := STV_SWF(prefs1)
	res2, _ := STV_SWF(prefs2)

	t.Log(prefs1)
	t.Log(res1)
	t.Log(prefs2)
	t.Log(res2)

	if res1[1] != 3 {
		t.Errorf("error, result for 1 should be 3, %d computed", res1[1])
	}
	if res1[2] != 1 {
		t.Errorf("error, result for 2 should be 1, %d computed", res1[2])
	}
	if res1[3] != 1 {
		t.Errorf("error, result for 3 should be 1, %d computed", res1[3])
	}

	if res2[2] != 4 {
		t.Errorf("error, result for 2 should be 4, %d computed", res2[2])
	}
	if res2[3] != 3 {
		t.Errorf("error, result for 3 should be 3, %d computed", res2[3])
	}
}

func TestSTV_SCF(t *testing.T) {
	prefs1 := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	prefs2 := [][]Alternative{
		{4, 2, 3, 1},
		{1, 2, 3, 4},
		{2, 3, 1, 4},
		{2, 3, 1, 4},
		{3, 1, 2, 4},
		{3, 2, 1, 4},
		{3, 2, 1, 4},
	}

	res1, _ := CopelandSCF(prefs1)
	res2, _ := CopelandSCF(prefs2)

	t.Log(prefs1)
	t.Log(res1)
	t.Log(prefs2)
	t.Log(res2)

	if len(res1) == 0 || res1[0] != 1 {
		t.Errorf("error, 1 should be the only best alternative for prefs1")
	}
	if len(res2) == 0 || res2[0] != 2 {
		t.Errorf("error, 2 should be the only best alternative for prefs2")
	}
}
