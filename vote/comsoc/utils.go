package comsoc

import "fmt"

// renvoie l'indice ou se trouve alt dans prefs
func rank(alt Alternative, prefs []Alternative) int {
	for i, a := range prefs {
		if a == alt {
			return i
		}
	}
	return -1
}

// renvoie vrai ssi alt1 est préférée à alt2
func isPref(alt1, alt2 Alternative, prefs []Alternative) bool {
	for _, a := range prefs {
		if a == alt1 {
			return true
		}

		if a == alt2 {
			return false
		}
	}
	return false
}

// renvoie les meilleures alternatives pour un décomtpe donné
func maxCount(count Count) (bestAlts []Alternative) {
	bestAlts = []Alternative{}
	max_score := -1

	for k, v := range count {
		if max_score == v {
			bestAlts = append(bestAlts, k)
		}
		if max_score < v {
			bestAlts = []Alternative{k}
			max_score = v
		}
	}
	return bestAlts
}

// vérifie les préférences d'un agent, par ex. qu'ils sont tous complets et que chaque alternative n'apparaît qu'une seule fois
func checkProfile(prefs []Alternative, alts []Alternative) error {
	m := make(map[Alternative]int)

	for _, a := range alts {
		m[a] = 0
	}

	for _, a := range prefs {
		_, exist := m[a]
		if exist == true {
			delete(m, a)
		} else {
			return fmt.Errorf("l'alternative %d n'est pas présente dans les alternatives possibles ou apparait plusieures fois", a)
		}
	}

	if len(m) != 0 {
		return fmt.Errorf("les préférences ne sont pas completes")
	}

	return nil
}

// vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative de alts apparaît exactement une fois par préférences
func checkProfileAlternative(prefs Profile, alts []Alternative) error {
	for _, pref := range prefs {
		err := checkProfile(pref, alts)
		if err != nil {
			return err
		}
	}
	return nil
}
