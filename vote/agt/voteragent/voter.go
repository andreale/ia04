package voteragent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	goballot "gitlab.utc.fr/andreale/ia04/vote"
	comsoc "gitlab.utc.fr/andreale/ia04/vote/comsoc"
)

type RestClientAgent struct {
	agentId string
	url     string
}

func NewRestClientAgent(agentId string, url string) *RestClientAgent {
	return &RestClientAgent{agentId, url}
}

func (rca *RestClientAgent) GetAgentId() string {
	return rca.agentId
}

func (rca *RestClientAgent) DoRequestNewBallot(rule string, deadline string, votersIds []string, alts int, tieBreak []comsoc.Alternative) (res string, err error) {
	req := goballot.NewBallotRqst{
		Rule:      rule,
		EndDate:   deadline,
		VotersIds: votersIds,
		NumbAlts:  alts,
		TieBreak:  tieBreak,
	}

	// sérialisation de la requête
	url := rca.url + "/new_ballot"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	var response goballot.NewBallotResp
	json.Unmarshal(buf.Bytes(), &response)

	res = response.BallotId

	return
}

func (rca *RestClientAgent) DoRequestVote(ballotId string, prefs []comsoc.Alternative, option []int) (err error) {
	var req goballot.VoteRqst
	if option != nil {
		req = goballot.VoteRqst{
			AgentId:  rca.agentId,
			BallotId: ballotId,
			Prefs:    prefs,
			Option:   option,
		}
	} else {
		req = goballot.VoteRqst{
			AgentId:  rca.agentId,
			BallotId: ballotId,
			Prefs:    prefs,
		}
	}

	// sérialisation de la requête
	url := rca.url + "/vote"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}

	return
}

func (rca *RestClientAgent) DoRequestResult(ballotId string) (winner comsoc.Alternative, ranking []comsoc.Alternative, err error) {
	req := goballot.ResultRqst{
		BallotId: ballotId,
	}

	// sérialisation de la requête
	url := rca.url + "/result"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		log.Println(err)
		return
	}

	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	var response goballot.VoteResp
	json.Unmarshal(buf.Bytes(), &response)

	winner = response.Winner
	ranking = response.Ranking

	return
}
