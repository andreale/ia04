package restserveragent

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"

	goballot "gitlab.utc.fr/andreale/ia04/vote"
)

type NewBallotTestRqst struct {
	Rule      string   `json:"rule"`
	EndDate   string   `json:"deadline"`
	VotersIds []string `json:"voter-ids"`
	NumbAlts  int      `json:"#alts"`
	TieBreak  []int    `json:"tie-break"`
}

type VoteTestRqst struct {
	AgentId  string `json:"agent-id"`
	BallotId string `json:"ballot-id"`
	Prefs    []int  `json:"prefs"`
	Option   []int  `json:"options,omitempty"`
}

func TestCreateTenNewBallot(t *testing.T) {
	rsa := NewRestServerAgent(":8080")

	ts := httptest.NewServer(http.HandlerFunc(rsa.newBallot))
	defer ts.Close()

	votersIds := []string{"ag_id1", "ag_id2", "ag_id3"}
	tieBreak := []int{4, 2, 3, 5, 9, 8, 7, 1, 6, 11, 12, 10}
	req := NewBallotTestRqst{
		Rule:      "majority",
		EndDate:   "3023-11-09T23:05:08+02:00",
		VotersIds: votersIds,
		NumbAlts:  12,
		TieBreak:  tieBreak,
	}

	data, _ := json.Marshal(req)

	log.Printf(string(data))
	for i := 1; i <= 10; i++ {
		res, err := http.Post(ts.URL, "application/json", bytes.NewBuffer(data))
		if err != nil {
			t.Fatal(err)
		}
		defer res.Body.Close()

		if res.StatusCode != http.StatusCreated {
			t.Errorf("Expected status code %d, got %d", http.StatusCreated, res.StatusCode)
		}

		buf := new(bytes.Buffer)
		buf.ReadFrom(res.Body)
		var resp goballot.NewBallotResp
		json.Unmarshal(buf.Bytes(), &resp)

		balotid := "scrutin" + strconv.Itoa(i)
		log.Printf(resp.BallotId)
		if resp.BallotId != balotid {
			t.Errorf("Error, resp of the request should be %s, %s computed", balotid, resp.BallotId)
		}
	}
}

func TestNewBallotWithVoterInDouble(t *testing.T) {
	rsa := NewRestServerAgent(":8080")
	ts := httptest.NewServer(http.HandlerFunc(rsa.newBallot))
	defer ts.Close()

	votersIds := []string{"ag_id1", "ag_id2", "ag_id3", "ag_id4", "ag_id1", "ag_id5"}
	tieBreak := []int{4, 2, 3, 5, 9, 8, 7, 1, 6, 11, 12, 10}
	req := NewBallotTestRqst{
		Rule:      "majority",
		EndDate:   "Mon Jan _2 15:04:05 MST 2006",
		VotersIds: votersIds,
		NumbAlts:  12,
		TieBreak:  tieBreak,
	}

	data, _ := json.Marshal(req)

	res, err := http.Post(ts.URL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, res.StatusCode)
	}

	responseBody, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}
	log.Printf(string(responseBody))
}

func TestInvalidTieBreaks(t *testing.T) {
	rsa := NewRestServerAgent(":8080")
	ts := httptest.NewServer(http.HandlerFunc(rsa.newBallot))
	defer ts.Close()

	votersIds := []string{"ag_id1", "ag_id2", "ag_id3"}
	tieBreak := []int{4, 2, 3, 5, 9, 8, 7, 1, 6, 11, 10}
	req := NewBallotTestRqst{
		Rule:      "majority",
		EndDate:   "Mon Jan _2 15:04:05 MST 2006",
		VotersIds: votersIds,
		NumbAlts:  12,
		TieBreak:  tieBreak,
	}

	data, _ := json.Marshal(req)

	res, err := http.Post(ts.URL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, res.StatusCode)
	}

	responseBody, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}
	log.Printf(string(responseBody))

	req = NewBallotTestRqst{
		Rule:      "majority",
		EndDate:   "Mon Jan _2 15:04:05 MST 2006",
		VotersIds: []string{"ag_id1", "ag_id2", "ag_id3"},
		NumbAlts:  12,
		TieBreak:  []int{4, 2, 3, 5, 9, 8, 7, 1, 13, 6, 11, 10, 12},
	}

	data, _ = json.Marshal(req)

	res, err = http.Post(ts.URL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, res.StatusCode)
	}

	responseBody, err = io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}
	log.Printf(string(responseBody))
}

func TestNewBallotWithEndDateTooEarly1(t *testing.T) {
	strEndDate := "2022-11-09T23:05:08+02:00"
	rsa := NewRestServerAgent(":8080")

	ts := httptest.NewServer(http.HandlerFunc(rsa.newBallot))
	defer ts.Close()

	votersIds := []string{"ag_id1", "ag_id2", "ag_id3"}
	tieBreak := []int{4, 2, 3, 5, 9, 8, 7, 1, 6, 11, 12, 10}
	req := NewBallotTestRqst{
		Rule:      "majority",
		EndDate:   strEndDate,
		VotersIds: votersIds,
		NumbAlts:  12,
		TieBreak:  tieBreak,
	}

	data, _ := json.Marshal(req)

	res, err := http.Post(ts.URL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, res.StatusCode)
	}

	responseBody, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}
	log.Printf(string(responseBody))
}

func TestNewBallotWithWrongDateFormat(t *testing.T) {
	rsa := NewRestServerAgent(":8080")
	ts := httptest.NewServer(http.HandlerFunc(rsa.newBallot))
	defer ts.Close()

	votersIds := []string{"ag_id1", "ag_id2", "ag_id3"}
	tieBreak := []int{4, 2, 3, 5, 9, 8, 7, 1, 6, 11, 12, 10}
	req := NewBallotTestRqst{
		Rule:      "majority",
		EndDate:   "Mon Jan _2 15:04:05 MST 2006",
		VotersIds: votersIds,
		NumbAlts:  12,
		TieBreak:  tieBreak,
	}

	data, _ := json.Marshal(req)

	res, err := http.Post(ts.URL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, res.StatusCode)
	}

	responseBody, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}
	log.Printf(string(responseBody))
}

func TestNewBallotWithUnimplementedRule(t *testing.T) {
	rule := "random"
	rsa := NewRestServerAgent(":8080")

	ts := httptest.NewServer(http.HandlerFunc(rsa.newBallot))
	defer ts.Close()

	votersIds := []string{"ag_id1", "ag_id2", "ag_id3"}
	tieBreak := []int{4, 2, 3, 5, 9, 8, 7, 1, 6, 11, 12, 10}
	req := NewBallotTestRqst{
		Rule:      rule,
		EndDate:   "3023-11-09T23:05:08+02:00",
		VotersIds: votersIds,
		NumbAlts:  12,
		TieBreak:  tieBreak,
	}

	data, _ := json.Marshal(req)

	res, err := http.Post(ts.URL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusNotImplemented {
		t.Errorf("Expected status code %d, got %d", http.StatusNotImplemented, res.StatusCode)
	}

	responseBody, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	log.Printf(string(responseBody))

}

func TestRegularVote(t *testing.T) {
	rsa := NewRestServerAgent(":8080")

	ts1 := httptest.NewServer(http.HandlerFunc(rsa.newBallot))
	ts2 := httptest.NewServer(http.HandlerFunc(rsa.vote))
	defer ts1.Close()
	defer ts2.Close()

	votersIds := []string{"ag_id1", "ag_id2", "ag_id3"}
	tieBreak := []int{4, 2, 3, 5, 9, 8, 7, 1, 6, 11, 12, 10}
	req1 := NewBallotTestRqst{
		Rule:      "majority",
		EndDate:   "3023-11-09T23:05:08+02:00",
		VotersIds: votersIds,
		NumbAlts:  12,
		TieBreak:  tieBreak,
	}

	data, _ := json.Marshal(req1)

	http.Post(ts1.URL, "application/json", bytes.NewBuffer(data))

	req := VoteTestRqst{
		AgentId:  "ag_id1",
		BallotId: "scrutin1",
		Prefs:    []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12},
	}
	data, _ = json.Marshal(req)

	log.Printf(string(data))

	res, err := http.Post(ts2.URL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected status code %d, got %d", http.StatusOK, res.StatusCode)
	}
}

func TestVoteWithWrongPrefs(t *testing.T) {
	rsa := NewRestServerAgent(":8080")

	ts1 := httptest.NewServer(http.HandlerFunc(rsa.newBallot))
	ts2 := httptest.NewServer(http.HandlerFunc(rsa.vote))
	defer ts1.Close()
	defer ts2.Close()

	votersIds := []string{"ag_id1", "ag_id2", "ag_id3"}
	tieBreak := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}
	req1 := NewBallotTestRqst{
		Rule:      "majority",
		EndDate:   "3023-11-09T23:05:08+02:00",
		VotersIds: votersIds,
		NumbAlts:  12,
		TieBreak:  tieBreak,
	}

	data, _ := json.Marshal(req1)

	http.Post(ts1.URL, "application/json", bytes.NewBuffer(data))

	req := VoteTestRqst{
		AgentId:  "ag_id1",
		BallotId: "scrutin1",
		Prefs:    []int{1, 2, 3, 4, 5, 6, 8, 8, 9, 10, 11, 12},
	}
	data, _ = json.Marshal(req)

	res, err := http.Post(ts2.URL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, res.StatusCode)
	}

	responseBody, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	log.Printf(string(responseBody))

	req = VoteTestRqst{
		AgentId:  "ag_id1",
		BallotId: "scrutin1",
		Prefs:    []int{1, 2, 3, 4, 5, 6, 8, 7, 14, 9, 10, 11, 12},
	}
	data, _ = json.Marshal(req)

	res, err = http.Post(ts2.URL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, res.StatusCode)
	}

	responseBody, err = io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	log.Printf(string(responseBody))
}

func TestVoteApprovalWrongOptions(t *testing.T) {
	rsa := NewRestServerAgent(":8080")

	ts1 := httptest.NewServer(http.HandlerFunc(rsa.newBallot))
	ts2 := httptest.NewServer(http.HandlerFunc(rsa.vote))
	defer ts1.Close()
	defer ts2.Close()

	votersIds := []string{"ag_id1", "ag_id2", "ag_id3"}
	tieBreak := []int{4, 2, 3, 5, 9, 8, 7, 1, 6, 11, 12, 10}
	req1 := NewBallotTestRqst{
		Rule:      "approval",
		EndDate:   "3023-11-09T23:05:08+02:00",
		VotersIds: votersIds,
		NumbAlts:  12,
		TieBreak:  tieBreak,
	}

	data, _ := json.Marshal(req1)

	http.Post(ts1.URL, "application/json", bytes.NewBuffer(data))

	req := VoteTestRqst{
		AgentId:  "ag_id1",
		BallotId: "scrutin1",
		Prefs:    []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12},
		Option:   []int{},
	}
	data, _ = json.Marshal(req)

	log.Printf(string(data))

	res, err := http.Post(ts2.URL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, res.StatusCode)
	}

	responseBody, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	log.Printf(string(responseBody))

	req = VoteTestRqst{
		AgentId:  "ag_id1",
		BallotId: "scrutin1",
		Prefs:    []int{1, 2, 3, 4, 5, 6, 8, 7, 9, 10, 11, 12},
		Option:   []int{14},
	}
	data, _ = json.Marshal(req)

	res, err = http.Post(ts2.URL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, res.StatusCode)
	}

	responseBody, err = io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	log.Printf(string(responseBody))
}

func TestVoterVotingTwoTimes(t *testing.T) {
	agentId := "ag_id1"
	rsa := NewRestServerAgent(":8080")

	ts1 := httptest.NewServer(http.HandlerFunc(rsa.newBallot))
	ts2 := httptest.NewServer(http.HandlerFunc(rsa.vote))
	defer ts1.Close()
	defer ts2.Close()

	votersIds := []string{"ag_id1", "ag_id2", "ag_id3"}
	tieBreak := []int{4, 2, 3, 5, 9, 8, 7, 1, 6, 11, 12, 10}
	req1 := NewBallotTestRqst{
		Rule:      "majority",
		EndDate:   "3023-11-09T23:05:08+02:00",
		VotersIds: votersIds,
		NumbAlts:  12,
		TieBreak:  tieBreak,
	}

	data, _ := json.Marshal(req1)

	http.Post(ts1.URL, "application/json", bytes.NewBuffer(data))

	req := VoteTestRqst{
		AgentId:  agentId,
		BallotId: "scrutin1",
		Prefs:    []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12},
	}
	data, _ = json.Marshal(req)

	res, err := http.Post(ts2.URL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected status code %d, got %d", http.StatusOK, res.StatusCode)
	}

	res, err = http.Post(ts2.URL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusForbidden {
		t.Errorf("Expected status code %d, got %d", http.StatusForbidden, res.StatusCode)
	}

	responseBody, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	log.Printf(string(responseBody))
}

func TestVoteOnNonCreatedBallot(t *testing.T) {
	ballotId := "scrutin10"
	rsa := NewRestServerAgent(":8080")

	ts1 := httptest.NewServer(http.HandlerFunc(rsa.newBallot))
	ts2 := httptest.NewServer(http.HandlerFunc(rsa.vote))
	defer ts1.Close()
	defer ts2.Close()

	votersIds := []string{"ag_id1", "ag_id2", "ag_id3"}
	tieBreak := []int{4, 2, 3, 5, 9, 8, 7, 1, 6, 11, 12, 10}
	req1 := NewBallotTestRqst{
		Rule:      "majority",
		EndDate:   "3023-11-09T23:05:08+02:00",
		VotersIds: votersIds,
		NumbAlts:  12,
		TieBreak:  tieBreak,
	}

	data, _ := json.Marshal(req1)

	http.Post(ts1.URL, "application/json", bytes.NewBuffer(data))

	req := VoteTestRqst{
		AgentId:  "ag_id1",
		BallotId: ballotId,
		Prefs:    []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12},
	}
	data, _ = json.Marshal(req)

	res, err := http.Post(ts2.URL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	// voir en fonction de ce qui est def dans server
	if res.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, res.StatusCode)
	}

	responseBody, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	log.Printf(string(responseBody))
}

func TestWrongVoter(t *testing.T) {
	wrongVoter := "ag_id4"
	ballotId := "scrutin1"
	rsa := NewRestServerAgent(":8080")

	ts1 := httptest.NewServer(http.HandlerFunc(rsa.newBallot))
	ts2 := httptest.NewServer(http.HandlerFunc(rsa.vote))
	defer ts1.Close()
	defer ts2.Close()

	votersIds := []string{"ag_id1", "ag_id2", "ag_id3"}
	tieBreak := []int{4, 2, 3, 5, 9, 8, 7, 1, 6, 11, 12, 10}
	req1 := NewBallotTestRqst{
		Rule:      "majority",
		EndDate:   "3023-11-09T23:05:08+02:00",
		VotersIds: votersIds,
		NumbAlts:  12,
		TieBreak:  tieBreak,
	}

	data, _ := json.Marshal(req1)

	http.Post(ts1.URL, "application/json", bytes.NewBuffer(data))

	req := VoteTestRqst{
		AgentId:  wrongVoter,
		BallotId: ballotId,
		Prefs:    []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12},
	}
	data, _ = json.Marshal(req)

	res, err := http.Post(ts2.URL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	// voir en fonction de ce qui est def dans server
	if res.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, res.StatusCode)
	}

	responseBody, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}
	log.Printf(string(responseBody))
}

func TestVoteTooLate(t *testing.T) {
	wrongVoter := "ag_id1"
	ballotId := "scrutin1"
	rsa := NewRestServerAgent(":8080")

	ts1 := httptest.NewServer(http.HandlerFunc(rsa.newBallot))
	ts2 := httptest.NewServer(http.HandlerFunc(rsa.vote))
	defer ts1.Close()
	defer ts2.Close()

	currentTime := time.Now()
	newTime := currentTime.Add(5 * time.Second)
	rfc3339Time := newTime.Format(time.RFC3339)

	votersIds := []string{"ag_id1", "ag_id2", "ag_id3"}
	tieBreak := []int{4, 2, 3, 5, 9, 8, 7, 1, 6, 11, 12, 10}
	req1 := NewBallotTestRqst{
		Rule:      "majority",
		EndDate:   rfc3339Time,
		VotersIds: votersIds,
		NumbAlts:  12,
		TieBreak:  tieBreak,
	}

	data, _ := json.Marshal(req1)

	http.Post(ts1.URL, "application/json", bytes.NewBuffer(data))

	req := VoteTestRqst{
		AgentId:  wrongVoter,
		BallotId: ballotId,
		Prefs:    []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12},
	}
	data, _ = json.Marshal(req)

	time.Sleep(5 * time.Second)
	res, err := http.Post(ts2.URL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusServiceUnavailable {
		t.Errorf("Expected status code %d, got %d", http.StatusServiceUnavailable, res.StatusCode)
	}

	responseBody, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}
	log.Printf(string(responseBody))
}

func TestMajorityResultDecidedWithTieBreak(t *testing.T) {
	rsa := NewRestServerAgent(":8080")

	ts1 := httptest.NewServer(http.HandlerFunc(rsa.newBallot))
	ts2 := httptest.NewServer(http.HandlerFunc(rsa.vote))
	tsResult := httptest.NewServer(http.HandlerFunc(rsa.result))
	defer ts1.Close()
	defer ts2.Close()
	defer tsResult.Close()

	currentTime := time.Now()
	newTime := currentTime.Add(5 * time.Second)
	rfc3339Time := newTime.Format(time.RFC3339)

	votersIds := []string{"ag_id1", "ag_id2", "ag_id3", "ag_id4"}
	tieBreak := []int{1, 2, 3}
	reqnewBallot := NewBallotTestRqst{
		Rule:      "majority",
		EndDate:   rfc3339Time,
		VotersIds: votersIds,
		NumbAlts:  3,
		TieBreak:  tieBreak,
	}

	data, _ := json.Marshal(reqnewBallot)

	http.Post(ts1.URL, "application/json", bytes.NewBuffer(data))

	prefs := [][]int{
		{1, 2, 3},
		{2, 3, 1},
		{3, 1, 2},
		{3, 1, 2},
	}

	for i, alts := range prefs {
		agentId := "ag_id" + strconv.Itoa(i+1)
		reqVote := VoteTestRqst{
			AgentId:  agentId,
			BallotId: "scrutin1",
			Prefs:    alts,
		}
		data, _ = json.Marshal(reqVote)
		log.Printf(string(data))
		http.Post(ts2.URL, "application/json", bytes.NewBuffer(data))
	}

	resultreq := goballot.ResultRqst{
		BallotId: "scrutin1",
	}
	data, _ = json.Marshal(resultreq)

	time.Sleep(5 * time.Second)
	resresult, err := http.Post(tsResult.URL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	defer resresult.Body.Close()

	if resresult.StatusCode != http.StatusOK {
		t.Errorf("Expected status code %d, got %d", http.StatusOK, resresult.StatusCode)
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(resresult.Body)
	var resp goballot.VoteResp
	json.Unmarshal(buf.Bytes(), &resp)

	log.Printf("resp: %v", resp)

	if resp.Winner != 3 {
		t.Errorf("error, resp of the request should be 1, %d computed", resp.Winner)
	}

	if resp.Ranking[0] != 3 || resp.Ranking[1] != 1 || resp.Ranking[2] != 2 {
		t.Errorf("error, resp of the request should be [3, 1, 2], %v computed", resp.Ranking)
	}

	resresult, err = http.Post(tsResult.URL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	defer resresult.Body.Close()

	if resresult.StatusCode != http.StatusOK {
		t.Errorf("Expected status code %d, got %d", http.StatusOK, resresult.StatusCode)
	}

	buf = new(bytes.Buffer)
	buf.ReadFrom(resresult.Body)
	json.Unmarshal(buf.Bytes(), &resp)

	log.Printf("resp: %v", resp)

	if resp.Winner != 3 {
		t.Errorf("error, resp of the request should be 1, %d computed", resp.Winner)
	}

	if resp.Ranking[0] != 3 || resp.Ranking[1] != 1 || resp.Ranking[2] != 2 {
		t.Errorf("error, resp of the request should be [3, 1, 2], %v computed", resp.Ranking)
	}
}

func TestApprovalResultDecidedWithTieBreak(t *testing.T) {
	rsa := NewRestServerAgent(":8080")

	ts1 := httptest.NewServer(http.HandlerFunc(rsa.newBallot))
	ts2 := httptest.NewServer(http.HandlerFunc(rsa.vote))
	tsResult := httptest.NewServer(http.HandlerFunc(rsa.result))
	defer ts1.Close()
	defer ts2.Close()
	defer tsResult.Close()

	currentTime := time.Now()
	newTime := currentTime.Add(5 * time.Second)
	rfc3339Time := newTime.Format(time.RFC3339)

	votersIds := []string{"ag_id1", "ag_id2", "ag_id3"}
	tieBreak := []int{1, 2, 3}
	reqnewBallot := NewBallotTestRqst{
		Rule:      "approval",
		EndDate:   rfc3339Time,
		VotersIds: votersIds,
		NumbAlts:  3,
		TieBreak:  tieBreak,
	}

	data, _ := json.Marshal(reqnewBallot)

	http.Post(ts1.URL, "application/json", bytes.NewBuffer(data))

	prefs := [][]int{
		{1, 2, 3},
		{1, 3, 2},
		{2, 3, 1},
	}
	option := [][]int{{2}, {1}, {2}}

	for i, alts := range prefs {
		agentId := "ag_id" + strconv.Itoa(i+1)
		reqVote := VoteTestRqst{
			AgentId:  agentId,
			BallotId: "scrutin1",
			Prefs:    alts,
			Option:   option[i],
		}
		data, _ = json.Marshal(reqVote)
		log.Printf(string(data))
		http.Post(ts2.URL, "application/json", bytes.NewBuffer(data))
	}

	resultreq := goballot.ResultRqst{
		BallotId: "scrutin1",
	}
	data, _ = json.Marshal(resultreq)

	time.Sleep(5 * time.Second)
	resresult, err := http.Post(tsResult.URL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	defer resresult.Body.Close()

	if resresult.StatusCode != http.StatusOK {
		t.Errorf("Expected status code %d, got %d", http.StatusOK, resresult.StatusCode)
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(resresult.Body)
	var resp goballot.VoteResp
	json.Unmarshal(buf.Bytes(), &resp)

	log.Printf("resp: %v", resp)

	if resp.Winner != 1 {
		t.Errorf("error, resp of the request should be 1, %d computed", resp.Winner)
	}

	if resp.Ranking[0] != 1 || resp.Ranking[1] != 2 || resp.Ranking[2] != 3 {
		t.Errorf("error, resp of the request should be [1, 2, 3], %v computed", resp.Ranking)
	}
}

func TestResultWithInvalidBallotId(t *testing.T) {
	rsa := NewRestServerAgent(":8080")

	ts1 := httptest.NewServer(http.HandlerFunc(rsa.newBallot))
	ts2 := httptest.NewServer(http.HandlerFunc(rsa.vote))
	tsResult := httptest.NewServer(http.HandlerFunc(rsa.result))
	defer ts1.Close()
	defer ts2.Close()
	defer tsResult.Close()

	currentTime := time.Now()
	newTime := currentTime.Add(120 * time.Second)
	rfc3339Time := newTime.Format(time.RFC3339)

	votersIds := []string{"ag_id1", "ag_id2", "ag_id3", "ag_id4"}
	tieBreak := []int{1, 2, 3}
	reqnewBallot := NewBallotTestRqst{
		Rule:      "majority",
		EndDate:   rfc3339Time,
		VotersIds: votersIds,
		NumbAlts:  3,
		TieBreak:  tieBreak,
	}

	data, _ := json.Marshal(reqnewBallot)

	http.Post(ts1.URL, "application/json", bytes.NewBuffer(data))

	prefs := [][]int{
		{1, 2, 3},
		{2, 3, 1},
		{3, 1, 2},
		{3, 1, 2},
	}

	for i, alts := range prefs {
		agentId := "ag_id" + strconv.Itoa(i+1)
		reqVote := VoteTestRqst{
			AgentId:  agentId,
			BallotId: "scrutin1",
			Prefs:    alts,
		}
		data, _ = json.Marshal(reqVote)
		http.Post(ts2.URL, "application/json", bytes.NewBuffer(data))
	}

	resultreq := goballot.ResultRqst{
		BallotId: "scrutin10",
	}
	data, _ = json.Marshal(resultreq)

	resresult, err := http.Post(tsResult.URL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}
	defer resresult.Body.Close()

	if resresult.StatusCode != http.StatusNotFound {
		t.Errorf("Expected status code %d, got %d", http.StatusNotFound, resresult.StatusCode)
	}

	responseBody, err := io.ReadAll(resresult.Body)
	if err != nil {
		t.Fatal(err)
	}

	log.Printf(string(responseBody))

}
