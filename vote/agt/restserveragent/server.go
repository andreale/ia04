package restserveragent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"sync"
	"time"

	goballot "gitlab.utc.fr/andreale/ia04/vote"
	comsoc "gitlab.utc.fr/andreale/ia04/vote/comsoc"
)

type RestServerAgent struct {
	sync.Mutex
	addr       string
	numBallots int
	ballots    map[string]*ballot
}

type ballot struct {
	ballotId     string
	rule         string
	startDate    time.Time
	endDate      time.Time
	voters       []string
	alreadyVoted []string
	profile      comsoc.Profile
	tieBreak     []comsoc.Alternative
	numbAlts     int
	options      [][]int
	winner       comsoc.Alternative
	ranking      []comsoc.Alternative
}

func checkAltsValid(Alts []comsoc.Alternative, NumbAlts int, w http.ResponseWriter) bool {
	if len(Alts) != NumbAlts {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "There are too many alternatives (%d when NumbAlts=%d)\n", len(Alts), NumbAlts)
		return false
	}
	for i := 1; i <= NumbAlts; i++ {
		TieBreakValid := false
		for _, alt := range Alts {
			if int(alt) == i {
				TieBreakValid = true
				break
			}
		}
		if !TieBreakValid {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, "Alternative %d is not in the slice of alternatives given\n", i)
			return false
		}
	}
	return true
}

func NewRestServerAgent(addr string) *RestServerAgent {
	return &RestServerAgent{addr: addr, ballots: make(map[string]*ballot)}
}

func (rsa *RestServerAgent) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusNotImplemented)
		fmt.Fprintf(w, "method %q not implemented", r.Method)
		return false
	}
	return true
}

func isRuleImplemented(RuleTested string) bool {
	for _, RuleImplemented := range comsoc.RulesImplemented {
		if RuleImplemented == RuleTested {
			return true
		}
	}
	return false
}

func (*RestServerAgent) decodeNewBallotRqst(r *http.Request) (req goballot.NewBallotRqst, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)

	return
}

func (rsa *RestServerAgent) newBallot(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	if !rsa.checkMethod("POST", w, r) {
		return
	}

	req, err := rsa.decodeNewBallotRqst(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// check unicity of voters
	for i, voterId := range req.VotersIds {
		for _, nextVoterId := range req.VotersIds[i+1:] {
			if voterId == nextVoterId {
				w.WriteHeader(http.StatusBadRequest)
				fmt.Fprintf(w, "A voter (%s) is several times in voter-ids", voterId)
				return
			}
		}
	}

	// check Tie Break is valid
	if !checkAltsValid(req.TieBreak, req.NumbAlts, w) {
		fmt.Fprintf(w, "Tie-break is not valid")
		return
	}

	// check end date format in RFC3339
	tEndDate, err := time.Parse(time.RFC3339, req.EndDate)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// check end date after current time
	tStartDate := time.Now()
	if tStartDate.After(tEndDate) {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "End date (%s) is already passed", req.EndDate)
		return
	}

	if !isRuleImplemented(req.Rule) {
		w.WriteHeader(http.StatusNotImplemented)
		fmt.Fprintf(w, "The rule (%s) has not been implemented", req.Rule)
		return
	}

	rsa.numBallots++

	newBallot := ballot{
		ballotId:     "scrutin" + strconv.Itoa(rsa.numBallots),
		rule:         req.Rule,
		startDate:    tStartDate,
		endDate:      tEndDate,
		voters:       req.VotersIds,
		alreadyVoted: make([]string, 0),
		profile:      make(comsoc.Profile, 0),
		tieBreak:     req.TieBreak,
		numbAlts:     req.NumbAlts,
		options:      make([][]int, 0),
		winner:       -1,
		ranking:      nil,
	}

	NewBallotResponse := goballot.NewBallotResp{
		BallotId: newBallot.ballotId,
	}

	rsa.ballots[newBallot.ballotId] = &newBallot

	w.WriteHeader(http.StatusCreated)
	serial, _ := json.Marshal(NewBallotResponse)
	w.Write(serial)
}

func (*RestServerAgent) decodeVoteRqst(r *http.Request) (req goballot.VoteRqst, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)

	return
}

func (rsa *RestServerAgent) vote(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	if !rsa.checkMethod("POST", w, r) {
		return
	}

	req, err := rsa.decodeVoteRqst(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// check the ballot exists
	ballot, exist := rsa.ballots[req.BallotId]
	if !exist {
		w.WriteHeader(http.StatusBadRequest) // ou Not Implemented
		fmt.Fprintf(w, "The ballot-id given (%s) doesn't correspond to a ballot created", req.BallotId)
		return
	}

	// check if the voter can vote
	voterValid := false
	for _, voter := range ballot.voters {
		if req.AgentId == voter {
			voterValid = true
			break
		}
	}
	if !voterValid {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "The voter given (%s) is not suposed to vote for this ballot (%s)", req.AgentId, req.BallotId)
		return
	}

	// check voter prefs are valid
	if !checkAltsValid(req.Prefs, ballot.numbAlts, w) {
		fmt.Fprintf(w, "Voter prefs are not valid")
		return
	}

	// check option is valid for approval vote
	if ballot.rule == "approval" {
		if len(req.Option) != 1 {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, "The option for approval vote should be one int (here %d)", len(req.Option))
			return
		}
		if req.Option[0] < 1 || req.Option[0] > ballot.numbAlts {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, "The option for approval vote should be between 1 and numbAlts (%d) here is %d ", ballot.numbAlts, req.Option[0])
			return
		}
	}

	// check ballot deadline is not over
	if ballot.endDate.Before(time.Now()) {
		w.WriteHeader(http.StatusServiceUnavailable)
		rfc3339Time := ballot.endDate.Format(time.RFC3339)
		fmt.Fprintf(w, "Deadline (%s) is passed", rfc3339Time)
		return
	}

	// check voter hasn't already voted
	for _, voter := range ballot.alreadyVoted {
		if req.AgentId == voter {
			w.WriteHeader(http.StatusForbidden)
			fmt.Fprintf(w, "The voter (%s) has already voted", req.AgentId)
			return
		}
	}

	ballot.alreadyVoted = append(ballot.alreadyVoted, req.AgentId)
	ballot.profile = append(ballot.profile, req.Prefs)
	if req.Option != nil {
		ballot.options = append(ballot.options, req.Option)
	}

	w.WriteHeader(http.StatusOK)
}

func (*RestServerAgent) decodeResultRqst(r *http.Request) (req goballot.ResultRqst, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)

	return
}

func (rsa *RestServerAgent) result(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	if !rsa.checkMethod("POST", w, r) {
		return
	}

	req, err := rsa.decodeResultRqst(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	ballot, exist := rsa.ballots[req.BallotId]
	if !exist {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "The ballot-id given (%s) doesn't correspond to a ballot created", req.BallotId)
		return
	}

	if ballot.endDate.After(time.Now()) {
		w.WriteHeader(http.StatusTooEarly)
		rfc3339Time := ballot.endDate.Format(time.RFC3339)
		fmt.Fprintf(w, "It's too early (end of vote: %s)", rfc3339Time)
		return
	}

	if ballot.winner == -1 || ballot.ranking != nil {
		tb := comsoc.TieBreakFactory(ballot.tieBreak)
		if len(ballot.options) == 0 {
			var scf func(comsoc.Profile) (comsoc.Alternative, error)
			var swf func(comsoc.Profile) ([]comsoc.Alternative, error)

			switch ballot.rule {
			case "majority":
				scf = comsoc.SCFFactory(comsoc.MajoritySCF, tb)
				swf = comsoc.SWFFactory(comsoc.MajoritySWF, tb)
			case "borda":
				scf = comsoc.SCFFactory(comsoc.BordaSCF, tb)
				swf = comsoc.SWFFactory(comsoc.BordaSWF, tb)
			case "stv":
				scf = comsoc.SCFFactory(comsoc.STV_SCF, tb)
				swf = comsoc.SWFFactory(comsoc.STV_SWF, tb)
			case "copeland":
				scf = comsoc.SCFFactory(comsoc.CopelandSCF, tb)
				swf = comsoc.SWFFactory(comsoc.CopelandSWF, tb)
			}

			ranking, err := swf(ballot.profile)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprint(w, err.Error())
				return
			}

			winner, err := scf(ballot.profile)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprint(w, err.Error())
				return
			}

			ballot.winner = winner
			ballot.ranking = ranking
		} else {
			var scf func(comsoc.Profile, [][]int) (comsoc.Alternative, error)
			var swf func(comsoc.Profile, [][]int) ([]comsoc.Alternative, error)

			switch ballot.rule {
			case "approval":
				scf = comsoc.SCFFactoryOptions(comsoc.ApprovalSCF, tb)
				swf = comsoc.SWFFactoryOptions(comsoc.ApprovalSWF, tb)
			}

			winner, err := scf(ballot.profile, ballot.options)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprint(w, err.Error())
				return
			}

			ranking, err := swf(ballot.profile, ballot.options)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprint(w, err.Error())
				return
			}
			ballot.winner = winner
			ballot.ranking = ranking
		}
	}

	VoteResponse := goballot.VoteResp{
		Winner:  ballot.winner,
		Ranking: ballot.ranking,
	}

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(VoteResponse)
	w.Write(serial)
}

func (rsa *RestServerAgent) Start() {
	mux := http.NewServeMux()
	// specification's commandes
	mux.HandleFunc("/new_ballot", rsa.newBallot)
	mux.HandleFunc("/vote", rsa.vote)
	mux.HandleFunc("/result", rsa.result)

	s := &http.Server{
		Addr:           rsa.addr,
		Handler:        mux,
		ReadTimeout:    100 * time.Second,
		WriteTimeout:   100 * time.Second,
		MaxHeaderBytes: 1 << 20}

	log.Println("Listening on", rsa.addr)
	go log.Fatal(s.ListenAndServe())
}
