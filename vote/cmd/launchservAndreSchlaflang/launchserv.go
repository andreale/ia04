package main

import (
	"fmt"
	"log"

	ras "gitlab.utc.fr/andreale/ia04/vote/agt/restserveragent"
)

func main() {
	log.Println("démarrage du serveur...")
	server := ras.NewRestServerAgent(":8080")
	go server.Start()
	fmt.Scanln()
}
