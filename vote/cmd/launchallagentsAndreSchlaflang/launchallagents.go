package main

import (
	"fmt"
	"log"
	"math/rand"
	"time"

	ras "gitlab.utc.fr/andreale/ia04/vote/agt/restserveragent"
	voteragent "gitlab.utc.fr/andreale/ia04/vote/agt/voteragent"
	comsoc "gitlab.utc.fr/andreale/ia04/vote/comsoc"
)

func shuffle(arr []comsoc.Alternative) []comsoc.Alternative {
	rand.Seed(time.Now().UnixNano())

	// Copy the entry tab to avoid mofying the original
	shuffled := make([]comsoc.Alternative, len(arr))
	copy(shuffled, arr)

	for i := len(shuffled) - 1; i > 0; i-- {
		j := rand.Intn(i + 1)
		shuffled[i], shuffled[j] = shuffled[j], shuffled[i]
	}

	return shuffled
}

func main() {
	const urlserveur = ":8080"
	const url = "http://localhost:8080"
	const nbvoterMax = 10
	const nbAltMax = 20
	RulesImplemented := comsoc.RulesImplemented
	server := ras.NewRestServerAgent(urlserveur)
	votersAgent := make([]*voteragent.RestClientAgent, 0, nbvoterMax)

	log.Println("Voter agents creation...")
	for i := 1; i <= nbvoterMax; i++ {
		id := fmt.Sprintf("agt_id%02d", i)
		agt := voteragent.NewRestClientAgent(id, url)
		votersAgent = append(votersAgent, agt)
	}
	log.Println("Voter agents ended")

	log.Println("démarrage du serveur...")
	go server.Start()

	time.Sleep(1 * time.Second) // wait the launch of the server

	for _, currentrule := range RulesImplemented {
		go func(rule string) {
			// random number between [2 nbAlts]
			nbvoter := rand.Intn(nbvoterMax-1) + 2
			votersIds := make([]string, nbvoter)
			for i := 0; i < nbvoter; i++ {
				id := fmt.Sprintf("agt_id%02d", i+1)
				votersIds[i] = id
			}

			// random number between [2 nbAlts]
			nbAlts := rand.Intn(nbAltMax-1) + 2
			Alternative := make([]comsoc.Alternative, nbAlts)
			for i := 0; i < nbAlts; i++ {
				Alternative[i] = comsoc.Alternative(i + 1)
			}
			tieBreak := shuffle(Alternative)

			now := time.Now()
			nextMinute := now.Add(5 * time.Second)
			tEndDate := nextMinute.Format(time.RFC3339)

			ballotId, err := votersAgent[0].DoRequestNewBallot(rule, tEndDate, votersIds, nbAlts, tieBreak)
			if err != nil {
				log.Fatal(err)
			}
			log.Printf("\n\nVote %s created: \n\trule=%v \n\ttEndDate=%v \n\tvotersIds=%v \n\talts=%v \n\ttieBreak=%v\n\n", ballotId, rule, tEndDate, votersIds, nbAlts, tieBreak)

			profil := make(comsoc.Profile, 0)
			options := make([][]int, 0)
			currvotersAgent := votersAgent[0:nbvoter]
			for _, agt := range currvotersAgent {
				// warning, it's mandatory to use this lambda function to catch iteration value for the go routine
				agtAlternative := shuffle(Alternative)
				profil = append(profil, agtAlternative)

				option := make([]int, 0)
				if rule == "approval" {
					option = append(option, rand.Intn(nbAlts-1)+1)
					options = append(options, option)
					log.Printf("Vote de l'agent %v dans le vote : %v avec les préference : %v et option: %v\n", agt.GetAgentId(), ballotId, agtAlternative, option)
				} else {
					log.Printf("Vote de l'agent %v dans le vote : %v avec les préference : %v\n", agt.GetAgentId(), ballotId, agtAlternative)
				}
				func(agt *voteragent.RestClientAgent) {
					go agt.DoRequestVote(ballotId, agtAlternative, option)
				}(agt)
			}

			log.Printf("Wait 5 sec the end of %s\n", ballotId)
			time.Sleep(5 * time.Second)

			winner, ranking, err := votersAgent[0].DoRequestResult(ballotId)
			if err != nil {
				log.Fatal(err)
			}

			var count comsoc.Count
			switch rule {
			case "majority":
				count, err = comsoc.MajoritySWF(profil)
			case "borda":
				count, err = comsoc.BordaSWF(profil)
			case "stv":
				count, err = comsoc.STV_SWF(profil)
			case "copeland":
				count, err = comsoc.CopelandSWF(profil)
			case "approval":
				count, err = comsoc.ApprovalSWF(profil, options)
			}

			if err != nil {
				log.Fatal(err)
			}
			if len(options) == 0 {
				log.Printf("\nResult vote %s:\nrule:%v \nprofil: %v \ncount: %v\ntie-break:%v \nwinner=%v and ranking=%v\n\n", ballotId, rule, profil, count, tieBreak, winner, ranking)
			} else {
				log.Printf("\nResult vote %s:\nrule:%v \nprofil: %v \noptions: %v \ncount: %v\ntie-break:%v  \nwinner=%v and ranking=%v\n\n", ballotId, rule, profil, options, count, tieBreak, winner, ranking)
			}

		}(currentrule)
	}

	fmt.Scanln()
}
